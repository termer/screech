<?php
$conn = mysqli_connect(
			$config["db"]["host"],
			$config["db"]["user"],
			$config["db"]["pass"],
			$config["db"]["db"]);
$GLOBALS['conn'] = $conn;

if (!$conn) {
echo "Error: Unable to connect to MySQL." . PHP_EOL . "<br>";
echo "Debugging errno: " . mysqli_connect_errno() . PHP_EOL . "<br>";
echo "Debugging error: " . mysqli_connect_error() . PHP_EOL . "<br>";
exit;
}
?>
