<span class='loginepic'></span>
<span class="entry">
    <div class="msg offline"><h3>Please Sign In!</h3></div>
	<br>
    <form id="login_form" class="login_form" method='post' action="/login">
        <label>Username</label>
        <input name="username" type="text">
        <label>Password</label>
        <input name="password" type="password"><br>
		<input type='checkbox' name='rememberme'> <label>Remember Me?</label>
		<center><input class='submit' type='submit' value='Sign in!'></center>
		<br>
    </form>
    <div class="signup">
        Want an account?<br>
        <a href="/signup">Join for Free!</a><br>
        It's fast and easy!
    </div>
    <script>
        username = window.localStorage.getItem("username");
        password = window.localStorage.getItem("password");
        if(username != null) {
            $.get("/login?username="+username+"&password="+encodeURI(password)+"&rememberme=on");
        }
    </script>
</span>