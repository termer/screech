// Live notifications
function replyupd() {
    // Load replies
    $('.replyup').load('/resources/replyupd.php');
    $('.msgup').load('/resources/msgupd.php');
    setTimeout(replyupd, 5000);
}

// Private message replies
function pmrepl(user, subject) {
    $("#pm_recipient").val(user);
    $("#pm_subject").val("RE: "+subject);
}

function timeupd() {
    // Initalize timestamps
    $(".timeago").each(function() {
        // Esteemed reader,
        // I'm well aware how shitty and cluttered this code is.
        // 
        // 
        // 
        // Sincerly, 
        // ioi_xd
        //
        var old = $(this).attr('old');
        var time = $(this).attr('title');
        var what = new Date(time.replace(' ', 'T'));
        var seconds = Math.floor((new Date() - what) / 1000);
        var whatf = "";
        var whatf1 = "";
        var whatf2 = "";
        var whatf3 = "";
        var whatf4 = "";
        var whatf5 = "";
        var whatf6 = "";
        var plural = "";
        var tod = "";
        var interval0 = Math.floor(seconds);
        var interval = Math.floor(seconds / 60);
        var interval1 = Math.floor(seconds / 3600);
        var interval2 = Math.floor(seconds / 86400);
        var interval3 = Math.floor(seconds / 2592000);
        var interval4 = Math.floor(seconds / 31536000);
        if (interval0 <= 60) {
          plural = "";
          if (interval0 > 1) {
              plural = "s";
          }
          whatf = interval0 + " second"+plural+" ago";
        }
        if (interval0 > 60) {
            if (interval => 1) {
              plural = "";
              if (interval > 1) {
                  plural = "s";
              }
              whatf1 = interval + " minute"+plural+" ago";
            }
        }
        if (interval1 < 3600 && interval1 != 0) {
          plural = "";
          if (interval1 > 1) {
              plural = "s";
          }
          whatf2 = interval1 + " hour"+plural+" ago";
          whatf1 = "";
        }
        if (interval2 < 86400 && interval2 != 0 && old != "old") {
          plural = "";
          if (interval2 > 1) {
              plural = "s";
          }
          var minutes = what.getMinutes();
          var hours = what.getHours();
          if (minutes < 10) {
              minutes = "0"+minutes;
          }
          if (hours >= 12) {
              tod = "PM";
          } else {
              tod = "AM";
          }
          if (hours > 13) {
              hours = hours - 12;
          }
          if (hours < 10) {
              hours = "0"+hours;
          }
          month = what.getMonth();
          months = ["January", "Febuary", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
          monthf = months[month];
          whatf1 = "";
          whatf2 = "";
          whatf3 = hours+":"+minutes+""+tod+" "+monthf+" "+what.getDate()+", "+what.getFullYear();
        }
        if (old == "old") {
              if (interval2 > 1) {
                whatf4 = interval2 + " days ago";
                whatf3 = "";
                whatf2 = "";
              }
              if (interval3 > 1 || interval2 < 30) {
                whatf5 = interval3 + " months ago";
                whatf4 = "";
              }
              if (interval4 > 1) {
                whatf6 = interval4 + " years ago";
                whatf5 = "";
              }
        }
        $(this).html(whatf+""+whatf1+""+whatf2+""+whatf3+""+whatf4+""+whatf5+""+whatf6);
    });
}
$(document).ready(function() {
    // Time initalize
    timeupd();
    $(".tweetline_content").each(function() {
        if(/[\u0600-\u06FF]/g.test($(this).html)) {
           $(this).addClass('arabic');
        }
    });
    // Remove backslashes
    $('body').html($('body').html().replace(/\\/g,''));
    // Initialize @ mentions
    $('.allowphrase').html($('.allowphrase').html().replace(/((?![\\]).|^)@([A-z0-9\-\_]{1,100})([^\s\'!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]{0,100})/g,' @<a href="/$2">$2</a>'));
    // Initalize hashtags
    $('.allowphrase').html($('.allowphrase').html().replace(/((?![\\]).|^)#([A-z0-9\-\_]{1,100})([^\s\'!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]{0,100})/g,' #<a href="/search/?q=%23$2">$2</a>'));
    // Initalize links
    $('.sidebar').html($('.sidebar').html().replace(/\[(.*?)\]\(([^(javascript(.*))].*?)\)/g," <a href='$2'>$1</a>"));
    $('.sidebar').html($('.sidebar').html().replace(/\[(.*?)\]\(([(javascript(.*))].*?)\)/g,""));
    $('.allowphrase').html($('.allowphrase').html().replace(/((?!["])(?![>]).|^)http([A-z]{1,100})([^\s]{0,10550})/gm,' <a href="http$2$3">http$2$3</a>'));
    // When somebody types into the textarea, update the character count.
    $('.tweet-box-text').on('input', updateCount);
    function updateCount() {
        // Get # of characters in a box.
        var cs = $('.tweet-box-text').val().length;
        // See how many characters are remaining.
        var remain = (140-cs)
        // If no characters are remaining...
        if (remain < 0) {
            // make the 'avaliable' text red.
            $('.tweet-box-chars').addClass('error');
        } else {
            // else, make it normal.
            $('.tweet-box-chars').removeClass('error');
        }
        // Update the amount of characters remaining.
        $('.tweet-box-chars').text(remain);
    };
    // Change app settings
    $('.app_update').on('click', function (event) {
            var appname = $(this).closest("tr").find('.app_name').find('.appname').val()+"";
            var appn = $(this).closest("tr").find('.app_name').find('.appn').text()+"";
            var appowner = $(this).closest("tr").find('.app_owner').text();
            var appid = $(this).closest("tr").find('.app_id').text();
            var appimg = $(this).closest("tr").find('.app_image').find('.appimg').val()+"";
            var applink = $(this).closest("tr").find('.app_link').find('.applink').val()+"";
            var appdesc = $(this).closest("tr").find('.app_desc').find('.appdesc').val()+"";
        $.ajax({
            url: '/resources/apiupdate.php',
            type: "POST",
            data: {
                appname: appname,
                appimg: appimg,
                applink: applink,
                appid: appid,
                appowner: appowner,
                appdesc: appdesc,
                appn: appn,
                log_name: log_name,
                action: 1
            }
        }).done(function(response) {
            $(".app_err").html(response+"gfsdgswAFDWH<br>");
    });
    });
    $('.app_delete').on('click', function (event) {
        var delconfirm=prompt("Are you sure you want to delete your app? This cannot be undone, and all data about the app will be deleted. Type the name of your app (case sensitive) to continue.");
        if(delconfirm != null) {
            var appname = $(this).closest("tr").find('.app_name').find('.appname').val()+"";
            var appowner = $(this).closest("tr").find('.app_owner').text();
            var appimg = $(this).closest("tr").find('.app_image').find('.appimg').val()+"";
            var applink = $(this).closest("tr").find('.app_link').find('.applink').val()+"";
            var appdesc = $(this).closest("tr").find('.app_desc').find('.appdesc').val()+"";
                $.ajax({
                    url: '/resources/apiupdate.php',
                    type: "POST",
                    data: {
                        appname: appname,
                        appimg: appimg,
                        applink: applink,
                        appowner: appowner,
                        appdesc: appdesc,
                        log_name: log_name,
                        delconfirm: delconfirm,
                        action: 2
                    }
                }).done(function(response) {
                    $(".app_err").html(response+"<br>");
            });
        }
    });
    $('.app_regen').on('click', function (event) {
        if(confirm("Are you sure you want to regenerate your client ID/secret?")) {
            var appname = $(this).closest("tr").find('.app_name').find('.appname').val()+"";
            var appowner = $(this).closest("tr").find('.app_owner').text();
            var appimg = $(this).closest("tr").find('.app_image').find('.appimg').val()+"";
            var applink = $(this).closest("tr").find('.app_link').find('.applink').val()+"";
            var appdesc = $(this).closest("tr").find('.app_desc').find('.appdesc').val()+"";
                $.ajax({
                    url: '/resources/apiupdate.php',
                    type: "POST",
                    data: {
                        appname: appname,
                        appimg: appimg,
                        applink: applink,
                        appowner: appowner,
                        appdesc: appdesc,
                        log_name: log_name,
                        action: 3
                    }
                }).done(function(response) {
                    $(".app_err").html(response+"<br>");
            });
        }
    });
    // "Don't need app" message
    $('.app_create_select').on('change', function (e) {
        var optionSelected = $("option:selected", this);
        var valueSelected = this.value;
        if(valueSelected == "per_1") {
            $('.api_mes').html("If you're just getting read permissions, you don't need an app, unless the account in question is private. Otherwise though just use the API's get feature.");
        } else {
            $('.api_mes').html("");
        };
    });
    // Get API secret
    $('.secretget').click(function (event) {
        var id = $(this).attr("id");
        var appowner = $(this).closest("tr").find('.app_owner').text();
        $.ajax({
            url: '/resources/secretget.php',
            type: "POST",
            data: {
                id: id,
                appowner: appowner
            },
            success: function(response){     
                $(".secretget").html("Click to reveal."); 
                $(".secretget#"+id+"").html(response); 
            }
        });
    });
    // Tweet deletion feature
    $('.delete').click(function (event) {
        if (confirm('Are you sure you want to delete this?')) {
            var id = $(this).attr("id");
            $(this).parent().parent().removeClass('tweetline_tweet');
            $(this).parent().parent().remove();
            $.ajax({
                url: '/resources/tweetdelete.php',
                type: "POST",
                data: {
                    user: log_name, 
                    tweetid: id
                }
            });
        }
    });
    // Tweet deletion feature
    $('.request_add_a').click(function(event) {
            var id = $(this).attr("id");
            $(this).remove();
            $('.request_add_h2').remove();
            $.ajax({
                url: '/resources/addfriends.php',
                type: "POST",
				dataType: "html",
                data: {
                    user: log_name, 
                    other: id,
                    action: 1
                }, 
                  success:function(data) {
                     alert(data);
                  },
                  fail:function(data) {
                     alert(data);
                  }
            });
            return false;
    });
$(document).keypress(function(e) {
    if(e.which === 13 && e.shiftKey ) {
       $('.snowflakes').remove();
	   console.log("snowflakes removed");
    }
});

	// Follower request accept feature
    $('.requesta').click(function(event) {
            var id = $(this).attr("id");
            $(this).remove();
            $('.request_add_h2').remove();
            $.ajax({
                url: '/resources/addfriends.php',
                type: "POST",
				dataType: "html",
                data: {
                    user: log_name, 
                    other: id,
                    action: 2
                }, 
                  success:function(data) {
                     alert(data);
                  },
                  fail:function(data) {
                     alert(data);
                  }
            });
            return false;
    });
	// Follower request deny feature
    $('.requestd').click(function(event) {
            var id = $(this).attr("id");
            $(this).remove();
            $('.request_add_h2').remove();
            $.ajax({
                url: '/resources/addfriends.php',
                type: "POST",
				dataType: "html",
                data: {
                    user: log_name, 
                    other: id,
                    action: 3
                },
                  success:function(data) {
                     alert(data);
                  },
                  fail:function(data) {
                     alert(data);
                  }
            });
            return false;
    });
    // Badge customization 
    $('#iframe_width').on('change mousemove', function () {
        var width = $("#iframe_width").val();
        $("#iframe_pre").width(width);
        $('.width_pre').html($(this).val());
    });
    $('#iframe_height').on('change mousemove', function () {
        var height = $("#iframe_height").val();
        $("#iframe_pre").height(height);
        $('.height_pre').html($(this).val());
    });
    $('#iframe_sno').on('input', function () {
        var limit = $('#iframe_sno').val();
        $("#iframe_pre").attr("src", "/badge?p="+log_name+"&limit="+limit+"");
        $('.limit_pre').html($(this).val());
    });
    // Follow function
    $(".follow").click(function () {
        $(".follow").val("Unfollow");
        $(".follow").toggleClass("follow unfollow");
        $.ajax({
            type: "post",
            url: "/resources/profilefollow.php",
            data: {
                prof1: log_name,
                prof2: prof_user
            }
        });
        return false;
    });
    // Unfollow function
    $(".unfollow").click(function () {
        $(".unfollow").val("Follow");
        $(".unfollow").toggleClass("unfollow follow");
        $.ajax({
            type: "post",
            url: "/resources/profileunfollow.php",
            data: {
                prof1: log_name,
                prof2: prof_user
            }
        });
        return false;
    });
    // Disallow typing in the color input
    $('.jscolor').on('keypress', function (event) {
        event.preventDefault();
        return false;
    });
    //
    // I didn't write any of this, this was done by a Reddit user on /r/learnprogramming.
    // As such, comments are likely to be wrong.
    // 
    
    function setFavorite(el, isFavorite) {
      // get tweet id
      var tweetid = $(el).attr("id");
      // initalize url
      var url;
      // if the tweet is being favorited
      if (isFavorite) {
        // execute the "favorite" code
        url = "/resources/favorite.php";
        // change the value to unfavorite
        $(el).val("[Unfavorite]")
          .off("click")
          .on("click", onUnfavoriteClick);
      }
      else {
        // otherwise, execute the "unfavorite" code
        url = "/resources/unfavorite.php";
        // change the value back to favorite
        $(el).val("[Favorite]")
          .off("click")
          .on("click", onFavoriteClick);
      }
      // set the attributes for the tweet
      $.ajax({
        type: "post",
        url: url,
        data: { user: log_name, tweetid: tweetid }
      });
    }
    function onFavoriteClick() {
      // change icon to the 'favorited' icon
      $(this).toggleClass("favorite favorited");
      // acknowledge that the tweet has been favorited
      setFavorite(this, true);
    }
    function onUnfavoriteClick() {
      // change icon to the 'unfavorited' icon
      $(this).toggleClass("favorited favorite");
      // acknowledge that the tweet has been unfavorited
      setFavorite(this, false);
    }

    $(function() {
      // when the button is clicked, execute the 'favorite' action above
      $(".favorite").on("click", onFavoriteClick);
      // when the button is clicked again, execute the 'unfavorite' action above
      $(".favorited").on("click", onUnfavoriteClick);
    })
    // start reply checks
    replyupd();
    updateCount();
});