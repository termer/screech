<?php
// windows xp award
if (strpos($_SERVER['HTTP_USER_AGENT'], "Windows NT 5.1") !== false) {
    if($globaluserinf['award_xp'] == "0") {
        echo "<span class='award_notif'>You got a secret award: 
        <b>Browsed Screech on Windows XP</b>. Theres no real award, 
        but your name is now on a <a href='/awards?award=xp'>semi-secret page</a> along with other people who got the same award!
        </span>";
        mysqli_query($conn, "UPDATE users SET `award_xp`=1 WHERE username = '$log_name'");
    }
} 
?>
<span class='replyup'>
</span>
<h2 style='color: black;'>What are we doing?</h2>
<?php
    $datetime = date_default_timezone_get();
    $usercontent = "";
    $timelinesql = "SELECT * FROM following WHERE user1 = '$log_name'";
    $timelineresult = mysqli_query($conn, $timelinesql);
    $personaltweetsql = "SELECT * FROM tweets WHERE username = '$log_name'";
    $personaltweetresult = mysqli_query($conn, $personaltweetsql);
    $favoritesql = "SELECT tweetid from favorites WHERE user = '$log_name'";
    $favoriteresult = mysqli_query($conn, $favoritesql);
    $following=array();
    $favorites=array();
    $tweetno_all = $personalfavoriteno = $personaltweetno = 0;
    while($personaltweetrow = mysqli_fetch_assoc($personaltweetresult)) {
        foreach ($personaltweetrow as $key=>$value) {
            if (strpos($value, $personaltweetrow['username']) === false) {
                continue;
            }
            $personaltweetno = $personaltweetno + 1;
        }
    }
    if (!$favoriteresult) {
        printf("Timeline error: %s\n", mysqli_error($conn));
    }
    while($timelinerow = mysqli_fetch_assoc($timelineresult)) {
        foreach ($timelinerow as $key=>$value) {
            if (strpos($value, $timelinerow['user2']) === false) {
                continue;
            }
            if(!in_array($value, $following)){
                $following[]=$timelinerow['user2'];
            }
        }
    }
    $following[]=$log_name;
    while($favoriterow = mysqli_fetch_assoc($favoriteresult)) {
        foreach ($favoriterow as $key=>$value) {
            if(!in_array($value, $favorites)){
                $favorites[]=$value;
                $personalfavoriteno = $personalfavoriteno + 1;
            }
        }
    }
    $p = $_GET['p'];
    if (empty($p)) {
        $p = 1;
    }
    $tweetlimit = 15;
    $offset = (($p - 1) * $tweetlimit);
    $tweet_query_all = "SELECT * FROM `tweets`";
    $tweet_results_all = mysqli_query($conn, $tweet_query_all);
    while($tweetlinerow_all = mysqli_fetch_assoc($tweet_results_all)) {
        foreach ($tweetlinerow_all as $key=>$value) {
            if(!in_array($tweetlinerow_all['username'], $following)){
                continue;
            }
            if (strpos($value, $tweetlinerow_all["tweet"]) === false) {
                continue;
            }
            $tweetno_all = $tweetno_all + 1;
        }
    }
    if ($_SESSION['likes_replies'] == 0) {
        $tweetlinesql = "SELECT * FROM tweets WHERE `tweet` NOT RLIKE '@([^\s\<]+)' AND `username` IN ('" . implode("','", $following) . "') ORDER BY CAST(id as SIGNED INTEGER) DESC LIMIT $tweetlimit OFFSET $offset";
    } else {
        $tweetlinesql = "SELECT * FROM tweets WHERE `username` IN ('" . implode("','", $following) . "') ORDER BY CAST(id as SIGNED INTEGER) DESC LIMIT $tweetlimit OFFSET $offset";
    }
    $tweetlineresult = mysqli_query($conn, $tweetlinesql);
    $pages = ceil($tweetno_all / $tweetlimit);
    
    if ($pages <= 0 and $tweetno_all >= 1) {
        $pages = 1;
    }
    if (!$tweetlineresult) {
        echo("<em>Nothing, you haven't followed anyone yet.</em>");
    }
    while($tweetlinerow = mysqli_fetch_assoc($tweetlineresult)) {
        foreach ($tweetlinerow as $key=>$value) {
            $favorite = "<span id='".$tweetlinerow["id"]."' class='tweetbutton favorite'>[Favorite]</span>";
            if(!in_array($tweetlinerow['username'], $following)){
                if ($tweetlinerow['username'] == $log_name) {
                    echo "";
                } else {
                    continue;
                }
            }
            if (strpos($value, $tweetlinerow["tweet"]) === false) {
                continue;
            }
            if(in_array($tweetlinerow["id"], $personalfavourings)){
                $favorite = "<span id='".$tweetlinerow["id"]."' class='tweetbutton favorited'>[Unfavorite]</span>";
            }
            if($tweetlinerow["username"] == $log_name) {
                $usercontent = "<span class='tweetbutton delete' id='".$tweetlinerow["id"]."'>[Delete]</span>";
            } else {
                $usercontent = "";
            }
            $aquery = mysqli_query($conn, "SELECT * FROM `apps` WHERE `appname` = '".$tweetlinerow["sentfrom"]."'");
            $aresult = mysqli_fetch_assoc($aquery);
            if(mysqli_num_rows($aquery) == 1) {
                $sentfrom = "<a href='".$aresult['applink']."'>".$tweetlinerow["sentfrom"]."</a>";
            } else {
                $sentfrom = $tweetlinerow["sentfrom"];
            }
            echo("
                <div class='tweetline_tweet'>
                    <span class='tweetline_prof'>
                        <img width='100%' height='100%' src='/profiles/images/".$tweetlinerow["username"].".png'>
                    </span>
                    <span class='tweetline_content'>
                        <b><a href='".$tweetlinerow["username"]."'>".$tweetlinerow["username"]."</a></b> ".$tweetlinerow["tweet"]."
                        <a href='/statuses/".$tweetlinerow["id"]."''><span class='timeago' title='".$tweetlinerow["timestamp"]."".$globaluserinf['timezone']."'>".$tweetlinerow["timestamp"]."</span></a> from $sentfrom
                         $favorite $usercontent
                </div><br>");
        }
    }
        echo ("<span class='allupdates_ctrl'>");
            if ($pages >= 1 && $pages <= 1) {
                if ($p == 1) {
                    echo "<span class='pagesel'>1</span> ";
                } else {
                    echo "<a class='pagesel' href='/?p=1'>1</a> ";
                }
            }
            if ($pages >= 2 && $pages <= 2) {
                if ($p == 2) {
                    echo "<span class='pagesel'>2</span> ";
                } else {
                    echo "<a class='pagesel' href='/?p=2'>2</a> ";
                }
            }
            if ($pages >= 3 && $pages <= 3) {
                if ($p == 3) {
                    echo "<span class='pagesel' >3</span> ";
                } else {
                    echo "<a class='pagesel' href='/?p=3'>3</a> ";
                }
            }
            if ($pages >= 4){
                echo "
                <a class='pagesel' href='/?p=".($p - 1)."'>« previous</a> ";
                if($pages > $p) {
                    echo "<span class='pagesel'>".$p."</span>";
                }
                
                if($pages > $p + 1) {
                    echo "<a class='pagesel' href='/?p=".($p + 1)."'>".($p + 1)."</a>";
                }
                if($pages > $p + 2) {
                    echo "<a class='pagesel' href='/?p=".($p + 2)."'>".($p + 2)."</a>";
                }
            }
            if ($pages >= 4) {
                if ($p != $pages) {
                    echo "<span>... <a href='/?p=$pages'>$pages</a></span>";
                    echo "<a class='pagesel' href='/?p=".($p + 1)."'>next »</a> ";
                } else {
                    echo "<span>... $pages</span>";
                }
            }
        echo ("</span>");
?>