<b>What are you doing?</b><br>
<?php
    if($_SERVER["REQUEST_METHOD"] == "POST") {
        if (isset($_POST['tweet_post'])) {
                $tweetcontents = $_POST["tweet-box-text"];
                if (empty($tweetcontents)) {
                    $tweeterr = "We need something here.";
                }
                if (strlen($tweetcontents) > 140) {
                    $tweeterr = "Your screech is too long. Try again.";
                }
                if (empty($log_name)) {
                    $tweeterr = "Your session name is empty. Please log out then log back and try again.";
                }
                $dupcheck = mysqli_query($conn, "SELECT * FROM tweets WHERE tweet = '$tweetcontents' AND username = '$log_name' AND timestamp BETWEEN DATE_SUB(NOW(), INTERVAL 30 MINUTE) AND NOW();");
                if(mysqli_num_rows($dupcheck) >= 1) {
                    $tweeterr = "You screeched this same thing less then 30 minutes ago. Please try again.";
                }
                $spamcheck = mysqli_query($conn, "SELECT * FROM tweets WHERE username = '$log_name' AND timestamp BETWEEN DATE_SUB(NOW(), INTERVAL 30 SECOND) AND NOW();");
                if(mysqli_num_rows($spamcheck) >= 1) {
                    $tweeterr = "You screeched something less then 30 seconds ago. Please wait before trying again.";
                }
                if (empty($tweeterr)) {
                    $sql = "INSERT INTO tweets (username, tweet, sentfrom) VALUES (?, ?, ?)";
                    $okay = "mobile";
                    if($stmt = mysqli_prepare($conn, $sql)){
                        mysqli_stmt_bind_param($stmt, "sss", $log_name, mysqli_real_escape_string($conn, htmlspecialchars($tweetcontents)), $okay);
                        if(mysqli_stmt_execute($stmt)){
                            header("Location: /");
                        } else{
                            echo "Something went wrong. Please try again later.";
                        }
                    }
                }
            }
    }
?>
<form method='post' action=''>
        <input rows='4' name='tweet-box-text' class='tweet-box-text'><br>
        <span class='tweet_err error'><?php echo $tweeterr;?></span><br>
        <input type='submit' value='update' name='tweet_post' class='btn submit'><br>
</form>
<b>you + friends</b><br>
<?php
$tweetlimit = 100;
$offset = 0;
$follownum = 0;
$following = array();
$followingsql = "SELECT * from following where user1 = '$log_name'";
$followingresult = mysqli_query($conn, $followingsql);
while($followingrow = mysqli_fetch_assoc($followingresult)) {
    foreach ($followingrow as $key=>$value) {
        if (strpos($value, $followingrow['user2']) === false) {
            continue;
        }
        $following[]=$followingrow['user2'];
    }
}

$following[]=$log_name;
$usersql = "SELECT * FROM users WHERE username = '$log_name'";
$userresult = mysqli_query($conn, $usersql);
$userrow = mysqli_fetch_array($userresult);
$feedsql = "SELECT * FROM tweets WHERE `username` IN ('". implode("','", $following) ."') ORDER BY CAST(id as SIGNED INTEGER) DESC LIMIT $tweetlimit OFFSET $offset";
$feedresult = mysqli_query($conn, $feedsql);
if(!$feedresult) {
    printf("Feed error: %s\n", mysqli_error($conn));
}
if(!$userresult) {
    printf("Userinfo error: %s\n", mysqli_error($conn));
}
echo "<ul>";
while($feedrow = mysqli_fetch_assoc($feedresult)) {
    foreach ($feedrow as $key=>$value) {
        if (strpos($value, $feedrow["tweet"]) === false) {
            continue;
        }
        $fallbackdate = date_parse($feedrow['timestamp']);
        $fallbackampm = "AM";
        $months = ["January,February,March,April,May,June,July,August,September,October,November,December"];
        $month = $months[$fallbackdate['month']];
        if ($fallbackdate['hour'] >= 12) {
            $fallbackampm = "PM";
        }
        if ($fallbackdate['hour'] >= 13) {
            $fallbackdate['hour'] = $fallbackdate['hour'] - 12;
        }
        if ($fallbackdate['hour'] < 10) {
            $fallbackdate['hour'] = "0".$fallbackdate['hour'];
        }
        if ($fallbackdate['minute'] < 10) {
            $fallbackdate['minute'] = "0".$fallbackdate['minute'];
        }
        $screech = $feedrow['tweet'];
        $screech = str_replace("\\","",$screech);
        $screech = preg_replace("/((?![\"]).|^)http([A-z]{1,100})([^\s]{0,10550})/m", " <a target='a_blank' href=\"http$2$3\">http$2$3</a>", $screech);
        $fallbackd = $fallbackdate['hour'].":".$fallbackdate['minute'].$fallbackampm.", ".$month." ".$fallbackdate['day'].", ".$fallbackdate['year'];
        echo("<li> <a class='prof_link' href='/".$feedrow["username"]."'>".$feedrow["username"]."</a>: $screech <small class='timeago' title='".$feedrow["timestamp"]."".$userrow["timezone"]."'>$fallbackd</small></li>");
        }
        
    }
    echo "</ul>";
?>