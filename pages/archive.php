<span class='homepage_tabs'>
    <a href="archive" class='tab selected archive'>Archive</a>
    <a href="replies" class='tab replies'>Replies</a>
    <a href="/" class='tab recent'>Recent</a>
</span>
<p>I have no idea what this page did, but it's in one of the screenshots of old Twitter (see: <a href='reference'>references used</a>) so I added it. If you happen to know what this page was let me know.</p><br>
<p>It MAY have been for getting archives of your tweets, but I kind of doubt that since I feel like it would be placed smaller near the RSS area. If you do want an archive of your tweets, <a href='https://archive.is'>use archive.is.</a> Otherwise, this area remains blank.</p><br>